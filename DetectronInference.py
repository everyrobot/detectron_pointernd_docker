from detectron2.engine import DefaultPredictor
from detectron2.utils.visualizer import Visualizer
from detectron2.evaluation import inference_on_dataset
import cv2
from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog, build_detection_test_loader
from detectron2.data.datasets import register_coco_instances
from detectron2.evaluation import COCOEvaluator
import os
import yaml
import numpy as np
import time
import glob
from pycocotools import mask


class DetectronInference():
    """
    This class instantiates detectron. As long as it exists, each call to detect() will be done on the same instance, thus
    reducingin initilization time.
    """

    def __init__(self, inputs_dir_path):
        self.base_path = os.getcwd()
        self.inputs_dir_path = inputs_dir_path

        # load config file path
        config_list = glob.glob(os.path.join(inputs_dir_path, "*.yaml"))
        config_list = [yaml_file for yaml_file in config_list if 'class_names' not in yaml_file]
        print("Config List: ")
        print(config_list)
        assert len(
            config_list) == 1, 'ERROR: Exactly one .yaml file has to be present in detectron2_configs directory, not counting class_names.yaml'
        self.config_file_path = config_list[0]

        # load weights path
        weights_list = glob.glob(os.path.join(inputs_dir_path, "*.pth"))
        print("Weight List: ")
        print(weights_list)
        assert len(
            weights_list) == 1, 'ERROR: Exactly one .pth weights file has to be present in detectron2_inference_weights ' \
                                'directory '
        self.weights_path = weights_list[0]
        # set config and weights
        self.config_file = get_cfg()
        self.config_file.merge_from_file(self.config_file_path)
        self.config_file.MODEL.WEIGHTS = self.weights_path
        self.config_file.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5
        self.labels_list = self.get_label_list()
        self.predictor = DefaultPredictor(self.config_file)
        assert len(self.labels_list) == int(self.config_file.get('MODEL').get('ROI_HEADS').get('NUM_CLASSES')), \
            "This model requires " + str(self.config_file.get('MODEL').get('ROI_HEADS').get('NUM_CLASSES')) + \
            " class labels but " + str(len(self.labels_list)) + " were provided"

    def detect_image(self, image):
        """
        This method does inference on a provided image using Detectron2.
        :param image: input image in opencv format
        :return: dict containing classes, boxes, masks and scores for all detections in order
        """
        inf_start = time.time()
        predictions = self.predictor(image)

        inf_end = time.time()
        print("Detectron object inference: " + str(inf_end - inf_start))

        res_start = time.time()
        outputs = predictions['instances'].get_fields()
        masks = outputs['pred_masks'].cpu().numpy().astype(np.uint8)
        encoded = list()
        for x in masks:
            res = mask.encode(np.asfortranarray(x))
            res['counts'] = res['counts'].decode()
            encoded.append(res)
        MetadataCatalog.get(self.config_file.DATASETS.TRAIN[0]).thing_classes = self.get_label_list()

        v = Visualizer(image, MetadataCatalog.get(self.config_file.DATASETS.TRAIN[0]), scale=1.2)
        out = v.draw_instance_predictions(predictions["instances"].to("cpu"))
        annotated_image = out.get_image()[:, :, ::-1]
        annotated_image = cv2.cvtColor(annotated_image, cv2.COLOR_RGB2BGR)
        _, encoded_annotated_image = cv2.imencode(".jpg", annotated_image)

        outputs = {
            "classes": [self.labels_list[class_digit] for class_digit in
                        outputs['pred_classes'].cpu().numpy().tolist()],
            # "bboxes": outputs['pred_boxes'].tensor.cpu().numpy().tolist(),
            "masks": encoded,
            "scores": outputs['scores'].cpu().numpy().tolist(),
            "class_ids": self.labels_list,
            "annotated_image": encoded_annotated_image.tolist()
        }
        res_end = time.time()
        print("Detectron object gpu result retrieval: ", str(res_end - res_start))
        return outputs

    def get_label_list(self):
        with open(os.path.join(self.inputs_dir_path, "class_names.yaml"), 'r') as stream:
            try:
                labels_list = yaml.safe_load(stream)['MODEL']['NAMES']
                return labels_list
            except yaml.YAMLError as exc:
                print(exc)

    def visualizer_inference(self, img_root, imgs_from_root, image_format, DATASET_ROOT):

        img_pths = glob.glob(img_root + '/*.' + image_format)
        self.config_file.MODEL.WEIGHTS = self.weights_path
        register_coco_instances("train_set", {}, DATASET_ROOT + '/train_images/AvenaDatasetTrain.json',
                                DATASET_ROOT + "/train_images")
        register_coco_instances("val_set", {}, DATASET_ROOT + "/val_images/AvenaDatasetVal.json",
                                DATASET_ROOT + "/val_images")
        MetadataCatalog.get("train_set").set(
            thing_classes=self.get_label_list())
        MetadataCatalog.get("val_set").set(
            thing_classes=self.get_label_list())
        predictor = self.predictor
        self.config_file.DATASETS.TRAIN = ("train_set",)
        self.config_file.DATASETS.TEST = ("val_set",)
        self.config_file.DATASETS.VAL = ("val_set",)
        for image in img_pths[-imgs_from_root:]:
            im = cv2.imread(image)
            im = cv2.resize(im, (1920, 1080))
            outputs = predictor(im)
            # We can use `Visualizer` to draw the predictions on the image.
            v = Visualizer(im[:, :, ::-1], MetadataCatalog.get(self.config_file.DATASETS.TRAIN[0]), scale=1.2)
            out = v.draw_instance_predictions(outputs["instances"].to("cpu"))
            cv2.imshow("image", out.get_image()[:, :, ::-1])
            cv2.waitKey()

    def evaluate(self, DATASET_ROOT):
        register_coco_instances("train_set", {}, DATASET_ROOT + '/train_images/AvenaDatasetTrain.json',
                                DATASET_ROOT + "/train_images")
        register_coco_instances("val_set", {}, DATASET_ROOT + "/val_images/AvenaDatasetVal.json",
                                DATASET_ROOT + "/val_images")

        MetadataCatalog.get("train_set").set(
            thing_classes=self.get_label_list())
        MetadataCatalog.get("val_set").set(
            thing_classes=self.get_label_list())

        evaluator = COCOEvaluator("val_set", self.config_file, False, output_dir="./eval_output/")
        val_loader = build_detection_test_loader(self.config_file, "val_set")
        inference_on_dataset(self.predictor.model, val_loader, evaluator)
