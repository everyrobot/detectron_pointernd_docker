FROM nvidia/cuda:11.1.1-cudnn8-runtime-ubuntu20.04

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
  python3-pip \
  git \
  screen \
  nano \
  psmisc \
  libgl1-mesa-glx 

# pytorch
RUN pip3 install --upgrade pip \
    && pip3 install setuptools==50.3.0 \
    && pip3 install --default-timeout=100 torch==1.7.1+cu110 torchvision==0.8.2+cu110 torchaudio===0.7.2 -f https://download.pytorch.org/whl/torch_stable.html \
    && pip install websockets \
    && pip install orjson \
    && pip install gdown \
    && pip install detectron2 -f https://dl.fbaipublicfiles.com/detectron2/wheels/cu110/torch1.7/index.html \
    && pip3 install opencv-python \
    && rm -rf /root/.cache/


RUN rm -rf /opt/avena
RUN mkdir -p /opt/avena
RUN git clone https://gitlab.com/everyrobot/detectron_pointernd_docker.git /opt/avena/detect_server

# Download detectron2 models weights and configs
RUN rm -rf /opt/avena/detect_server/ml_vision_input_folder && \
    mkdir -p /opt/avena/detect_server/ml_vision_input_folder && cd /opt/avena/detect_server/ml_vision_input_folder && \
    gdown https://drive.google.com/uc?id=1TIniUon_tv_MZP8cMAQuXIWGZZAhyPEc && \
    gdown https://drive.google.com/uc?id=1ozfPzKfcQgckUw5tPj_rIRZsaHEIq2zg && \
    gdown https://drive.google.com/uc?id=1Kv23QCGbZV-4v4foJ3CAPj7tPtsCxT8m

RUN apt-get install -y libgtk2.0-dev
RUN cd /opt/avena/detect_server && mkdir DATASETS && gdown https://drive.google.com/uc?id=1TIniUon_tv_MZP8cMAQuXIWGZZAhyPEc
ENTRYPOINT ["python3", "/opt/avena/detect_server/DetectronTrain.py"]

