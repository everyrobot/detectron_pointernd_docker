import yaml
import os
import glob
import detectron2.utils.comm as comm
from collections import OrderedDict
from detectron2.data.datasets import register_coco_instances
from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog, build_detection_test_loader
from detectron2.engine import DefaultTrainer, default_setup, hooks, launch, DefaultPredictor
from detectron2.evaluation import COCOEvaluator, inference_on_dataset
from detectron2.engine import HookBase
from detectron2.data import build_detection_train_loader
from detectron2.modeling import GeneralizedRCNNWithTTA
import logging
import torch
import sys
from point_rendd import point_rend
DATASET_ROOT = '/opt/avena/detect_server/blender_dataset'
NUM_GPUS = 1
INPUTS_DIR = '/opt/avena/detect_server/ml_vision_input_folder'

# load weights path
weights_list = glob.glob(os.path.join(INPUTS_DIR + "/*.pkl"))
##add point rend init weights###
# weights_list.append('/home/lewiatan/DETECTRON2_weights_configs/ml_vision_train_input_dir/model_final_ba17b9.pkl')
assert len(weights_list) == 1, 'ERROR: Exactly one .pth weights file has to be present in detectron2_inference_weights ' \
                               'directory '
INIT_WEIGHTS = weights_list

# load config file path
config_list = glob.glob(os.path.join(INPUTS_DIR + "/*.yaml"))
config_list = [yaml_file for yaml_file in config_list if 'class_names' not in yaml_file]
assert len(config_list) == 1, 'ERROR: Exactly one .yaml file has to be present in detectron2_configs directory, ' \
                              'not counting class_names.yaml '
CONFIG = config_list[0]


def get_label_list():
    with open(INPUTS_DIR + "/class_names_blender.yaml", 'r') as stream:
        try:
            labels_list = yaml.safe_load(stream)['MODEL']['NAMES']
            return labels_list
        except yaml.YAMLError as exc:
            print(exc)


class ValidationLoss(HookBase):
    def __init__(self, cfg):
        super().__init__()
        self.cfg = cfg.clone()
        self.cfg.DATASETS.TRAIN = cfg.DATASETS.VAL
        self._loader = iter(build_detection_train_loader(self.cfg))

    def after_step(self):
        data = next(self._loader)
        with torch.no_grad():
            loss_dict = self.trainer.model(data)

            losses = sum(loss_dict.values())
            assert torch.isfinite(losses).all(), loss_dict

            loss_dict_reduced = {"val_" + k: v.item() for k, v in
                                 comm.reduce_dict(loss_dict).items()}
            losses_reduced = sum(loss for loss in loss_dict_reduced.values())
            if comm.is_main_process():
                self.trainer.storage.put_scalars(total_val_loss=losses_reduced,
                                                 **loss_dict_reduced)


class Trainer(DefaultTrainer):
    """
    We use the "DefaultTrainer" which contains pre-defined default logic for
    standard training workflow. They may not work for you, especially if you
    are working on a new research project. In that case you can write your
    own training loop. You can use "tools/plain_train_net.py" as an example.
    """

    @classmethod
    def build_evaluator(cls, cfg, dataset_name, output_folder=None, distributed=True):
        return COCOEvaluator(dataset_name, cfg, distributed, output_folder)

    @classmethod
    def test_with_TTA(cls, cfg, model):
        logger = logging.getLogger("detectron2.trainer")
        # In the end of training, run an evaluation with TTA
        # Only support some R-CNN models.
        logger.info("Running inference with test-time augmentation ...")
        model = GeneralizedRCNNWithTTA(cfg, model)
        evaluators = [
            cls.build_evaluator(
                cfg, name, output_folder=os.path.join(cfg.OUTPUT_DIR, "inference_TTA")
            )
            for name in cfg.DATASETS.TEST
        ]
        res = cls.test(cfg, model, evaluators)
        res = OrderedDict({k + "_TTA": v for k, v in res.items()})
        return res


def setup():
    """
    Create configs and perform basic setups.
    """
    cfg = get_cfg()
    register_coco_instances("train_set", {}, DATASET_ROOT + '/train/train.json',
                            DATASET_ROOT + "/train")
    register_coco_instances("val_set", {}, DATASET_ROOT + "/val/val.json",
                            DATASET_ROOT + "/val")

    MetadataCatalog.get("train_set").set(
        thing_classes=get_label_list())
    MetadataCatalog.get("val_set").set(
        thing_classes=get_label_list())

    ######################################################
    ########################POINTREND#####################
    point_rend.add_pointrend_config(cfg)
    cfg.merge_from_file(
        CONFIG)
    point_rend.add_pointrend_config(cfg)
    cfg.MODEL.WEIGHTS = INIT_WEIGHTS[0]
    cfg.MODEL.POINT_HEAD.NUM_CLASSES = 16
    # True if point head is used.
    cfg.MODEL.ROI_MASK_HEAD.POINT_HEAD_ON = True
    ######################################################
    ######################################################

    cfg.OUTPUT_DIR = './output/'
    cfg.MODEL.ROI_HEADS.NUM_CLASSES = 16
    cfg.SOLVER.CHECKPOINT_PERIOD = 2000
    cfg.DATALOADER.NUM_WORKERS = 3
    cfg.MODEL.BACKBONE.FREEZE_AT = 1
    # Sample size of smallest side by choice or random selection from range give by
    cfg.INPUT.MIN_SIZE_TRAIN_SAMPLING = "choice"
    cfg.INPUT.MIN_SIZE_TRAIN = (800,)
    # cfg.INPUT.MAX_SIZE_TRAIN = 1333 * (2/8)
    cfg.INPUT.MAX_SIZE_TRAIN = 1300
    # cfg.MODEL.ANCHOR_GENERATOR.SIZES = [[32, 64, 128, 256, 384]]
    # Apply Deformable Convolution in stages
    # Specify if apply deform_conv on Res2, Res3, Res4, Res5
    # cfg.MODEL.RESNETS.DEFORM_ON_PER_STAGE = [True, True, True, True]
    # Use True to use modulated deform_conv (DeformableV2, https://arxiv.org/abs/1811.11168);
    # Use False for DeformableV1.
    # cfg.MODEL.RESNETS.DEFORM_MODULATED = True
    cfg.MODEL.FPN.NORM = ""
    cfg.MODEL.ROI_MASK_HEAD.NORM = ""
    cfg.MODEL.ROI_BOX_HEAD.NORM = ""
    cfg.MODEL.RESNETS.NORM = "FrozenBN"
    cfg.DATASETS.TRAIN = ("train_set",)
    cfg.DATASETS.TEST = ("val_set",)
    cfg.DATASETS.VAL = ("val_set",)
    cfg.SOLVER.IMS_PER_BATCH = 4
    cfg.SOLVER.BASE_LR = 0.0002 # pick a good LearningRate
    cfg.SOLVER.STEPS = (10000, 30000, 50000)
    cfg.SOLVER.GAMMA = 0.5
    cfg.SOLVER.WARMUP_FACTOR = 1.0 / 1000
    cfg.SOLVER.WARMUP_ITERS = 500
    cfg.SOLVER.WARMUP_METHOD = "linear"
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7
    cfg.SOLVER.MAX_ITER = 75000  # No. of iterations
    # cfg.TEST.EVAL_PERIOD = 500  # No. of iterations after which the Validation Set is evaluated.
    os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)
    cfg.DATASETS.TRAIN = ("train_set",)
    cfg.DATASETS.TEST = ("val_set",)
    cfg.DATASETS.VAL = ("val_set",)
    cfg.DATASETS.TRAIN = ("train_set",)
    cfg.DATASETS.TEST = ("val_set",)
    cfg.DATASETS.VAL = ("val_set",)
    default_setup(cfg, '')
    return cfg


def main():
    cfg = setup()
    """
    If you'd like to do anything fancier than the standard training logic,
    consider writing your own training loop (see plain_train_net.py) or
    subclassing the trainer.
    """
    trainer = Trainer(cfg)
    val_loss = ValidationLoss(cfg)
    trainer.register_hooks([val_loss])
    trainer._hooks = trainer._hooks[:-2] + trainer._hooks[-2:][::-1]
    trainer.build_evaluator(cfg, "val_set", './eval_output/')
    trainer.resume_or_load(resume=False)
    if cfg.TEST.AUG.ENABLED:
        trainer.register_hooks(
            [hooks.EvalHook(0, lambda: trainer.test_with_TTA(cfg, trainer.model))]
        )
    cfg.freeze()
    return trainer.train()

def evaluate_trained_model():
    cfg = setup()
    # """
    # If you'd like to do anything fancier than the standard training logic,
    # consider writing your own training loop (see plain_train_net.py) or
    # subclassing the trainer.
    # """
    # trainer = Trainer(cfg)
    # val_loss = ValidationLoss(cfg)
    # trainer.register_hooks([val_loss])
    # trainer._hooks = trainer._hooks[:-2] + trainer._hooks[-2:][::-1]
    # trainer.build_evaluator(cfg, "val_set", './eval_output/')
    # trainer.resume_or_load(resume=False)
    # if cfg.TEST.AUG.ENABLED:
    #     trainer.register_hooks(
    #         [hooks.EvalHook(0, lambda: trainer.test_with_TTA(cfg, trainer.model))]
    #     )
    # cfg.freeze()
    # return trainer.train()

    cfg.MODEL.WEIGHTS = CONFIG
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.70
    trainer = DefaultPredictor(cfg)
    evaluator = COCOEvaluator("val_set", cfg, False, output_dir="./output/")
    val_loader = build_detection_test_loader(cfg, "val_set")
    inference_on_dataset(trainer.model, val_loader, evaluator)

if __name__ == "__main__":
    args = sys.argv
    if args[1] == 'train':
        launch(
            main,
            NUM_GPUS,
            num_machines=1,
            machine_rank=0,
            dist_url='auto'
        )
    # evaluate_trained_model()
