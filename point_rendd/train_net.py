#!/usr/bin/env python3
# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved

"""
point_rend Training Script.

This script is a simplified version of the training script in detectron2/tools.
"""

import os
import torch

import detectron2.data.transforms as T
import detectron2.utils.comm as comm
from detectron2.checkpoint import DetectionCheckpointer
from detectron2.config import get_cfg
from detectron2.data import DatasetMapper, MetadataCatalog, build_detection_train_loader
from detectron2.engine import DefaultTrainer, default_argument_parser, default_setup, launch
from detectron2.evaluation import (
    CityscapesInstanceEvaluator,
    CityscapesSemSegEvaluator,
    COCOEvaluator,
    DatasetEvaluators,
    LVISEvaluator,
    SemSegEvaluator,
    verify_results,
)
from detectron2.data.datasets import register_coco_instances
from detectron2.projects import point_rend
from detectron2.projects.point_rend import ColorAugSSDTransform, PointRendROIHeads, CoarseMaskHead, PointRendSemSegHead, point_head, roi_heads, coarse_mask_head, point_features, config, color_augmentation, semantic_seg
import glob
import yaml

DATASET_ROOT = '/home/lewiatan/DATASETS'
NUM_GPUS = 4
INPUTS_DIR = '/home/lewiatan/DETECTRON2_weights_configs/ml_vision_train_input_dir'

# load weights path
weights_list = glob.glob(os.path.join(INPUTS_DIR + "/*weights.pkl"))
##add point rend init weights###
weights_list.append('/home/lewiatan/DETECTRON2_weights_configs/ml_vision_train_input_dir/model_final_ba17b9.pkl')
assert len(weights_list) == 2, 'ERROR: Exactly one .pth weights file has to be present in detectron2_inference_weights ' \
                               'directory '
INIT_WEIGHTS = weights_list

# load config file path
config_list = glob.glob(os.path.join(INPUTS_DIR + "/*.yaml"))
config_list = [yaml_file for yaml_file in config_list if 'class_names' not in yaml_file]
assert len(config_list) == 1, 'ERROR: Exactly one .yaml file has to be present in detectron2_configs directory, ' \
                              'not counting class_names.yaml '
CONFIG = config_list[0]


def get_label_list():
    with open(INPUTS_DIR + "/class_names.yaml", 'r') as stream:
        try:
            labels_list = yaml.safe_load(stream)['MODEL']['NAMES']
            return labels_list
        except yaml.YAMLError as exc:
            print(exc)



def build_sem_seg_train_aug(cfg):
    augs = [
        T.ResizeShortestEdge(
            cfg.INPUT.MIN_SIZE_TRAIN, cfg.INPUT.MAX_SIZE_TRAIN, cfg.INPUT.MIN_SIZE_TRAIN_SAMPLING
        )
    ]
    if cfg.INPUT.CROP.ENABLED:
        augs.append(
            T.RandomCrop_CategoryAreaConstraint(
                cfg.INPUT.CROP.TYPE,
                cfg.INPUT.CROP.SIZE,
                cfg.INPUT.CROP.SINGLE_CATEGORY_MAX_AREA,
                cfg.MODEL.SEM_SEG_HEAD.IGNORE_VALUE,
            )
        )
    if cfg.INPUT.COLOR_AUG_SSD:
        augs.append(ColorAugSSDTransform(img_format=cfg.INPUT.FORMAT))
    augs.append(T.RandomFlip())
    return augs


class Trainer(DefaultTrainer):
    """
    We use the "DefaultTrainer" which contains a number pre-defined logic for
    standard training workflow. They may not work for you, especially if you
    are working on a new research project. In that case you can use the cleaner
    "SimpleTrainer", or write your own training loop.
    """

    @classmethod
    def build_evaluator(cls, cfg, dataset_name, output_folder=None):
        """
        Create evaluator(s) for a given dataset.
        This uses the special metadata "evaluator_type" associated with each builtin dataset.
        For your own dataset, you can simply create an evaluator manually in your
        script and do not have to worry about the hacky if-else logic here.
        """
        if output_folder is None:
            output_folder = os.path.join(cfg.OUTPUT_DIR, "inference")
        evaluator_list = []
        evaluator_type = MetadataCatalog.get(dataset_name).evaluator_type
        if evaluator_type == "lvis":
            return LVISEvaluator(dataset_name, cfg, True, output_folder)
        if evaluator_type == "coco":
            return COCOEvaluator(dataset_name, cfg, True, output_folder)
        if evaluator_type == "sem_seg":
            return SemSegEvaluator(
                dataset_name,
                distributed=True,
                num_classes=cfg.MODEL.SEM_SEG_HEAD.NUM_CLASSES,
                ignore_label=cfg.MODEL.SEM_SEG_HEAD.IGNORE_VALUE,
                output_dir=output_folder,
            )
        if evaluator_type == "cityscapes_instance":
            assert (
                torch.cuda.device_count() >= comm.get_rank()
            ), "CityscapesEvaluator currently do not work with multiple machines."
            return CityscapesInstanceEvaluator(dataset_name)
        if evaluator_type == "cityscapes_sem_seg":
            assert (
                torch.cuda.device_count() >= comm.get_rank()
            ), "CityscapesEvaluator currently do not work with multiple machines."
            return CityscapesSemSegEvaluator(dataset_name)
        if len(evaluator_list) == 0:
            raise NotImplementedError(
                "no Evaluator for the dataset {} with the type {}".format(
                    dataset_name, evaluator_type
                )
            )
        if len(evaluator_list) == 1:
            return evaluator_list[0]
        return DatasetEvaluators(evaluator_list)

    @classmethod
    def build_train_loader(cls, cfg):
        if "SemanticSegmentor" in cfg.MODEL.META_ARCHITECTURE:
            mapper = DatasetMapper(cfg, is_train=True, augmentations=build_sem_seg_train_aug(cfg))
        else:
            mapper = None
        return build_detection_train_loader(cfg, mapper=mapper)


def setup(pointrend=True):
        """
        Create configs and perform basic setups.
        """
        cfg = get_cfg()

        if pointrend:
            point_rend.add_pointrend_config(cfg)

        ## POINTREND ##
        # add_pointrend_config(cfg)
        # cfg.MODEL.POINT_HEAD.NUM_CLASSES = 5

        register_coco_instances("train_set", {}, DATASET_ROOT + '/train/train.json',
                                DATASET_ROOT + "/train")
        register_coco_instances("val_set", {}, DATASET_ROOT + "/val/val.json",
                                DATASET_ROOT + "/val")

        MetadataCatalog.get("train_set").set(
            thing_classes=get_label_list())
        MetadataCatalog.get("val_set").set(
            thing_classes=get_label_list())
        if not point_rend:
            cfg.merge_from_file(CONFIG)
            cfg.MODEL.WEIGHTS = INIT_WEIGHTS[0]

        else:
            cfg.merge_from_file(
                '/home/lewiatan/Documents/detectron2_pointrend/projects/PointRend/configs/InstanceSegmentation/pointrend_rcnn_X_101_32x8d_FPN_3x_coco.yaml')
            point_rend.add_pointrend_config(cfg)
            cfg.MODEL.WEIGHTS = INIT_WEIGHTS[1]
            #####POINTREND####

            # True if point head is used.
            cfg.MODEL.ROI_MASK_HEAD.POINT_HEAD_ON = True

            #####/POINTREND#####

        cfg.OUTPUT_DIR = './output/'
        cfg.MODEL.WEIGHTS = INIT_WEIGHTS
        # cfg.MODEL.DEVICE = 'cuda:3'
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = 20
        cfg.SOLVER.CHECKPOINT_PERIOD = 500
        cfg.DATALOADER.NUM_WORKERS = 10
        cfg.MODEL.BACKBONE.FREEZE_AT = 0
        # Sample size of smallest side by choice or random selection from range give by
        cfg.INPUT.MIN_SIZE_TRAIN_SAMPLING = "choice"
        cfg.INPUT.MIN_SIZE_TRAIN = (800,)
        # cfg.INPUT.MAX_SIZE_TRAIN = 1333 * (2/8)
        cfg.INPUT.MAX_SIZE_TRAIN = 1333
        # cfg.MODEL.ANCHOR_GENERATOR.SIZES = [[32, 64, 128, 256, 384]]
        # Apply Deformable Convolution in stages
        # Specify if apply deform_conv on Res2, Res3, Res4, Res5
        # cfg.MODEL.RESNETS.DEFORM_ON_PER_STAGE = [True, True, True, True]
        # Use True to use modulated deform_conv (DeformableV2, https://arxiv.org/abs/1811.11168);
        # Use False for DeformableV1.
        # cfg.MODEL.RESNETS.DEFORM_MODULATED = True
        cfg.MODEL.FPN.NORM = ""
        cfg.MODEL.ROI_MASK_HEAD.NORM = ""
        cfg.MODEL.ROI_BOX_HEAD.NORM = ""
        cfg.MODEL.RESNETS.NORM = "FrozenBN"
        cfg.DATASETS.TRAIN = ("train_set",)
        cfg.DATASETS.TEST = ("val_set",)
        cfg.DATASETS.VAL = ("val_set",)
        cfg.SOLVER.IMS_PER_BATCH = 24
        cfg.SOLVER.BASE_LR = 0.006  # pick a good LearningRate
        cfg.SOLVER.STEPS = (150, 300, 400)
        cfg.SOLVER.GAMMA = 0.5
        cfg.SOLVER.WARMUP_FACTOR = 1.0 / 1000
        cfg.SOLVER.WARMUP_ITERS = 200
        cfg.SOLVER.WARMUP_METHOD = "linear"
        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7
        cfg.SOLVER.MAX_ITER = 500  # No. of iterations
        cfg.TEST.EVAL_PERIOD = 500  # No. of iterations after which the Validation Set is evaluated.
        os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)
        cfg.DATASETS.TRAIN = ("train_set",)
        cfg.DATASETS.TEST = ("val_set",)
        cfg.DATASETS.VAL = ("val_set",)
        cfg.DATASETS.TRAIN = ("train_set",)
        cfg.DATASETS.TEST = ("val_set",)
        cfg.DATASETS.VAL = ("val_set",)
        default_setup(cfg, '')
        return cfg


def main():
    cfg = setup(True)

    # if args.eval_only:
    #     model = Trainer.build_model(cfg)
    #     DetectionCheckpointer(model, save_dir=cfg.OUTPUT_DIR).resume_or_load(
    #         cfg.MODEL.WEIGHTS, resume=args.resume
    #     )
    #     res = Trainer.test(cfg, model)
    #     if comm.is_main_process():
    #         verify_results(cfg, res)
    #     return res

    trainer = Trainer(cfg)
    trainer.resume_or_load(resume=False)
    return trainer.train()


if __name__ == "__main__":
    # args = default_argument_parser().parse_args()
    # print("Command Line Args:", args)
    launch(
        main,
        4,
        num_machines=1,
        machine_rank=0,
        dist_url='auto',
    )
